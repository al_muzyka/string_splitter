package almu.testing;

import almu.testing.utils.Builder;
import almu.testing.utils.StructureBuilder;
import almu.testing.utils.StructureDirector;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        //  Get string from input stream
        System.out.print("Enter a string : ");
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.nextLine();
        inputString = prepareString(inputString); // todo, also skipped string validation

        //  Build the desired structure
        StructureBuilder builder = new StructureBuilder();
        StructureDirector director = new StructureDirector(builder);
        director.construct(inputString);

        // Print user-friendly format
        System.out.println(builder.getResult());
    }

    // It’s not clear what to do with uppercase.
    // Should it be handled otherwise?
    // Not enough requirements -> I have simplified this question so far
    public static String prepareString(String input) {
        return input.toLowerCase();
    }
}
